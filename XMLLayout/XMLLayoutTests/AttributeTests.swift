//
//  AttributeTests.swift
//  XMLLayoutTests
//
//  Created by Jarosław Krajewski on 01/08/2018.
//  Copyright © 2018 com.jerronimo. All rights reserved.
//

import XCTest

class AttributeTests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    override func tearDown() {
        super.tearDown()
    }

    func testMarginAttributes() {
        let topMarginAttr = XMLLayoutAttribute(val: "margin.top")
        XCTAssert(topMarginAttr == XMLLayoutAttribute.margin(val: .top))

        let bottomMarginAttr = XMLLayoutAttribute(val: "margin.bottom")
        XCTAssert(bottomMarginAttr == XMLLayoutAttribute.margin(val: .bottom))

        let leadingMarginAttr = XMLLayoutAttribute(val: "margin.leading")
        XCTAssert(leadingMarginAttr == XMLLayoutAttribute.margin(val: .leading))

        let trailingMarginAttr = XMLLayoutAttribute(val: "margin.trailing")
        XCTAssert(trailingMarginAttr == XMLLayoutAttribute.margin(val: .trailing))

        let allMarginAttr = XMLLayoutAttribute(val: "margin")
        XCTAssert(allMarginAttr == XMLLayoutAttribute.margin(val: .all))
    }

    func testLayoutAttributes(){
        let topLayoutAttr = XMLLayoutAttribute(val: "layout.top")
        XCTAssert(topLayoutAttr == XMLLayoutAttribute.layout(val: .top))

        let bottomLayoutAttr = XMLLayoutAttribute(val: "layout.bottom")
        XCTAssert(bottomLayoutAttr == XMLLayoutAttribute.layout(val: .bottom))

        let leadingLayoutAttr = XMLLayoutAttribute(val: "layout.leading")
        XCTAssert(leadingLayoutAttr == XMLLayoutAttribute.layout(val: .leading))

        let trailingLayoutAttr = XMLLayoutAttribute(val: "layout.trailing")
        XCTAssert(trailingLayoutAttr == XMLLayoutAttribute.layout(val: .trailing))

        let allLayoutAttr = XMLLayoutAttribute(val: "layout")
        XCTAssert(allLayoutAttr == XMLLayoutAttribute.layout(val: .all))
    }

    func testInsetAttributes() {
        let topInsetAttr = XMLLayoutAttribute(val: "inset.top")
        XCTAssert(topInsetAttr == XMLLayoutAttribute.inset(val: .top))

        let bottomInserAttr = XMLLayoutAttribute(val: "inset.bottom")
        XCTAssert(bottomInserAttr == XMLLayoutAttribute.inset(val: .bottom))

        let leadingInsetAttr = XMLLayoutAttribute(val: "inset.leading")
        XCTAssert(leadingInsetAttr == XMLLayoutAttribute.inset(val: .leading))

        let trailingInsetAttr = XMLLayoutAttribute(val: "inset.trailing")
        XCTAssert(trailingInsetAttr == XMLLayoutAttribute.inset(val: .trailing))

        let allInsetAttr = XMLLayoutAttribute(val: "inset")
        XCTAssert(allInsetAttr == XMLLayoutAttribute.inset(val: .all))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
