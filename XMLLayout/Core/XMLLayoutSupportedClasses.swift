//
//  XMLLayoutSupportedClasses.swift
//  XMLLayoutDesigner
//
//  Created by Jarosław Krajewski on 28/07/2018.
//

import Foundation
public enum XMLLayoutSupportedClasses: String, CaseIterable{
    case button = "Button"
    case view = "View"
    case label = "Label"
    case imageView = "ImageView"
}
