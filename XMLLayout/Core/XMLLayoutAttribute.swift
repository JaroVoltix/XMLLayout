import Foundation

public enum XMLLayoutAttribute:Equatable{
    public static var allAttributeNames: [String]{
        var basicAttr = basicAttribute.allCases.map{$0.rawValue}
        basicAttr.append(contentsOf:marginAttribute.allCases.map{"margin.\($0.rawValue)"})
        basicAttr.append(contentsOf:layoutAttribute.allCases.map{"layout.\($0.rawValue)"})
        basicAttr.append(contentsOf:customizationAttribute.allCases.map{$0.rawValue})
        basicAttr.append(contentsOf:insetAttribute.allCases.map{"inset.\($0.rawValue)"})
        basicAttr.append(contentsOf:shadowAttribute.allCases.map{"shadow.\($0.rawValue)"})
        return basicAttr
    }

    case margin(attr: marginAttribute)
    case basic(attr: basicAttribute)
    case layout(attr :layoutAttribute)
    case customization(attr: customizationAttribute)
    case inset(attr :insetAttribute)
    case shadow(attr: shadowAttribute)

    init?(val:String) {
        if val.hasPrefix("margin"){
            if let marginAttr = marginAttribute(value: val.components(separatedBy: "margin").last!){
                self = .margin(attr:marginAttr)
                return
            }
        } else if val.hasPrefix("layout"){
            if let layoutAttr = layoutAttribute(rawValue: val.components(separatedBy: "layout").last!.replacingOccurrences(of: ".", with: "")){
                self = .layout(attr:layoutAttr)
                return
            }
        } else if val.hasPrefix("inset"){
            if let insetAttr = insetAttribute(value: val.components(separatedBy: "inset").last!){
                self = .inset(attr:insetAttr)
                return
            }
        } else if val.hasPrefix("shadow"){
            let suffix = val.components(separatedBy: "shadow").last!
            if suffix.count > 0, let shadowAttr = shadowAttribute(rawValue: suffix.prefix(1).lowercased() + suffix.dropFirst()){
                self = .shadow(attr:shadowAttr)
                return
            }
        }
        else if let basicAttr = basicAttribute(rawValue:val) {
            self = .basic(attr: basicAttr)
            return
        } else if let customizationAttr = customizationAttribute(rawValue: val) {
            self  = .customization(attr: customizationAttr)
            return
        }
        return nil
    }

    public var priority:Int {
        switch self {
        case .margin(_):
            return 3
        case .basic(_):
            return 1
        case .layout(_):
            return 2
        case .customization(_):
            return 1
        case .inset(_):
            return 4
        case .shadow(_):
            return 5
        }
    }
    
    public enum basicAttribute: String,CaseIterable {
        case id
        case isHidden
        case text
        case fontSize
        case fontStyle
        case textAlignment
        case userInteractionEnabled
        case width
        case height
        case numberOfLines
        case lineBreakMode
        case image
        case contentMode

        case cornerRadius
        case borderWidth
    }

    public enum shadowAttribute: String,CaseIterable {
        case color
        case opacity
        case radius
        case offsetX
        case offsetY
    }

    public enum customizationAttribute: String,CaseIterable {
        case textColor
        case backgroundColor
        case alpha
        case borderColor
    }

    public enum marginAttribute:String,AttributePositionEnum,CaseIterable{
        case top
        case bottom
        case leading
        case trailing
        case all
    }

    public enum layoutAttribute:String,CaseIterable{
        case top
        case bottom
        case leading
        case trailing
        case centerX
        case centerY
    }

    public enum insetAttribute:String,AttributePositionEnum,CaseIterable {
        case top
        case bottom
        case leading
        case trailing
        case all
    }
}

protocol AttributePositionEnum{
    init?(rawValue:String)
    init?(value:String)
}

extension AttributePositionEnum {
    init?(value:String){
        var value = value.replacingOccurrences(of: ".", with: "")
        if value.isEmpty {
            value = "all"
        }
        self.init(rawValue: value)
    }
}
