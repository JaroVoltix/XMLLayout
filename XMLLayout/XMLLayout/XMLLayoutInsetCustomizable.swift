import UIKit

public protocol XMLLayoutInsetCustomizable{
    var inset: UIEdgeInsets {get set}
}

extension UIButton: XMLLayoutInsetCustomizable{
    public var inset: UIEdgeInsets {
        get {
            return contentEdgeInsets
        }
        set {
            contentEdgeInsets = newValue
        }
    }
}
