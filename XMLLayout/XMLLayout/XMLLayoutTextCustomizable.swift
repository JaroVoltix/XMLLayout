//
//  XMLLayoutTextCustomizable.swift
//  SWXMLHash iOS
//
//  Created by Jarosław Krajewski on 15/07/2018.
//

import UIKit

public protocol XMLLayoutTextCustomizable{
    
    func setTextFromXmlAttribute(text: String)
    func setTextColorFromXmlAttribute(color: UIColor)
    func setFontSizeFromXmlAttribute(size: CGFloat)
    func setFontStyleFromXmlAttribute(style: String)
    func setTextAlignmentFromXmlAttribute(alignment: String)
    func setNumberOflinesFromXmlAttribute(number:Int)
    func setLineBreakModeFromXmlAttribute(mode: NSLineBreakMode)
}

extension UIButton:XMLLayoutTextCustomizable{

    public func setTextFromXmlAttribute(text: String) {
        setTitle(text, for: .normal)
    }

    public func setTextColorFromXmlAttribute(color:UIColor){
        setTitleColor(color, for: .normal)
    }

    public func setFontSizeFromXmlAttribute(size: CGFloat) {
        guard let currentFont = titleLabel?.font else {
            titleLabel?.font = UIFont.systemFont(ofSize: size)
            return
        }
        self.titleLabel?.font = UIFont(name: currentFont.fontName, size: size)
    }

    public func setFontStyleFromXmlAttribute(style: String) {
        let currentFontSize: CGFloat
        if let fontSize = titleLabel?.font.pointSize {
            currentFontSize = fontSize
        } else {
            currentFontSize = UIFont.systemFontSize
        }
        guard let weight = UIFont.Weight.init(name: style) else {
            return
        }
        titleLabel?.font = UIFont.systemFont(ofSize: currentFontSize, weight: weight)
    }

    public func setTextAlignmentFromXmlAttribute(alignment: String) {
        titleLabel?.textAlignment = NSTextAlignment(name: alignment)
    }

    @objc public func setNumberOflinesFromXmlAttribute(number: Int) {
        titleLabel?.numberOfLines = number
    }

    public func setLineBreakModeFromXmlAttribute(mode: NSLineBreakMode) {
        titleLabel?.lineBreakMode = mode
    }
}

extension UILabel: XMLLayoutTextCustomizable {
    public func setTextFromXmlAttribute(text: String) {
        self.text = text
    }

    public func setTextColorFromXmlAttribute(color: UIColor) {
        self.textColor = color
    }

    public func setFontSizeFromXmlAttribute(size: CGFloat) {
        guard let currentFont = font else {
            self.font = UIFont.systemFont(ofSize: size)
            return
        }
        self.font = UIFont(name: currentFont.fontName, size: size)
    }

    public func setFontStyleFromXmlAttribute(style: String) {
        let currentFontSize: CGFloat
        if let fontSize = font?.pointSize {
            currentFontSize = fontSize
        } else {
            currentFontSize = UIFont.systemFontSize
        }
        guard let weight = UIFont.Weight.init(name: style) else {
            return
        }
        font = UIFont.systemFont(ofSize: currentFontSize, weight: weight)
    }

    public func setTextAlignmentFromXmlAttribute(alignment: String) {
        self.textAlignment = NSTextAlignment(name: alignment)
    }

    public func setNumberOflinesFromXmlAttribute(number: Int) {
        numberOfLines = number
    }

    public func setLineBreakModeFromXmlAttribute(mode: NSLineBreakMode) {
        lineBreakMode = mode
    }
}
