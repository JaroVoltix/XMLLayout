//
//  XMLLayoutImageSettable.swift
//  XMLLayout
//
//  Created by Jarosław Krajewski on 09/08/2018.
//  Copyright © 2018 com.jerronimo. All rights reserved.
//

import UIKit

protocol XMLLayoutImageSettable {
    func setImageFromXmlAttribute(imageName: String)
}

extension UIImageView :XMLLayoutImageSettable{
    func setImageFromXmlAttribute(imageName: String) {
        self.image = UIImage(named: imageName)
    }
}
