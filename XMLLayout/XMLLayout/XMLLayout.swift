//
//  XMLLayout.swift
//  XMLLayoutDesigner
//
//  Created by Jarosław Krajewski on 28/07/2018.
//

import UIKit
public class XMLLayout{
    private let xml:XMLIndexer
    private var views:[String:UIView] = [:]
    public init(xml:String){
        self.xml = SWXMLHash.parse(xml)
    }

    public func layout(view: UIView){
        let root = xml["Root"]
        createSubviewsAndAsignIds(rootElement:root, rootView:view)
        layout(superViewElement: root, in: view)
    }

    private func createSubviewsAndAsignIds(rootElement: XMLIndexer, rootView: UIView){
        for (_, element) in rootElement.children.enumerated(){
            let childView: UIView
            if let elementName = element.element?.name, let child = XMLLayoutSupportedClasses(rawValue: elementName)?.view{
                childView = child
            } else {
                childView = UIView()
            }
            childView.translatesAutoresizingMaskIntoConstraints = false
            rootView.addSubview(childView)
            createSubviewsAndAsignIds(rootElement: element, rootView: childView)
            guard let id = element.element?.allAttributes[XMLLayoutAttribute.basicAttribute.id.rawValue]?.text else {
                continue
            }
            childView.xmlViewTag = id
            views[id] = childView
        }
    }

    private func layout(superViewElement:XMLIndexer, in view:UIView){
        for (index, child) in superViewElement.children.enumerated(){
            let childView = view.subviews[index]
            childView.translatesAutoresizingMaskIntoConstraints = false

            child.element?.allAttributes
                .filter{key,value in return XMLLayoutAttribute(val:key) != nil}
                .sorted(by:{XMLLayoutAttribute(val:$0.key)!.priority < XMLLayoutAttribute(val:$1.key)!.priority})
                .forEach{(attrName,attrValue) in
                    guard let attr = XMLLayoutAttribute(val:attrName) else {
                        return
                    }
                    switch attr {

                    case .basic(let attr):
                        setupBasicAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    case .customization(let attr):
                        setupCustomizationAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    case .inset(let attr):
                        setupInsetAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    case .layout(let attr):
                        setupLayoutAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    case .margin(let attr):
                        setupMarginAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    case .shadow(let attr):
                        setupShadowAttribute(attr:attr,value:attrValue.text,childView:childView,superView:view)
                    }
            }
            layout(superViewElement: superViewElement.children[index], in: childView)
        }
    }

    private func setupBasicAttribute(attr:XMLLayoutAttribute.basicAttribute,value:String,childView:UIView,superView:UIView){
        switch attr{

        case .id:
            childView.xmlViewTag = value
            views[value] = childView
        case .image :
            guard let childView = childView as? XMLLayoutImageSettable else {
                return
            }
            childView.setImageFromXmlAttribute(imageName: value)
        case .contentMode:
            guard let mode = UIView.ContentMode(name:value) else {
                return
            }
            childView.contentMode = mode
        case .text:
            (childView as? XMLLayoutTextCustomizable)?.setTextFromXmlAttribute(text: value)
        case .fontSize:
            guard let size = value.asCGFloat else { return }
            (childView as? XMLLayoutTextCustomizable)?.setFontSizeFromXmlAttribute(size: size)
        case .fontStyle:
            (childView as? XMLLayoutTextCustomizable)?.setFontStyleFromXmlAttribute(style: value)
        case .textAlignment:
            (childView as? XMLLayoutTextCustomizable)?.setTextAlignmentFromXmlAttribute(alignment: value)
        case .numberOfLines:
            guard let number = Int(value) else {
                return
            }
            (childView as? XMLLayoutTextCustomizable)?.setNumberOflinesFromXmlAttribute(number: number)
        case .lineBreakMode:
            guard let breakMode = NSLineBreakMode(name: value) else {
                return
            }
            (childView as? XMLLayoutTextCustomizable)?.setLineBreakModeFromXmlAttribute(mode: breakMode)
        case .userInteractionEnabled:
            guard let boolValue = value.asBool else { return }
            childView.isUserInteractionEnabled = boolValue
        case .isHidden:
            guard let boolValue = value.asBool else { return}
            childView.isHidden = boolValue
        case .width:
            guard let width = value.asCGFloat else { return }
            let constaraint = childView.widthAnchor.constraint(equalToConstant: width)
            constaraint.isActive = true
        case .height:
            guard let height = value.asCGFloat else { return }
            let constaraint = childView.heightAnchor.constraint(equalToConstant: height)
            constaraint.isActive = true
        case .cornerRadius:
            guard let radius = value.asCGFloat else { return }
            childView.layer.cornerRadius = radius
            childView.layer.masksToBounds = true
        case .borderWidth:
            guard let width = value.asCGFloat else { return }
            childView.layer.borderWidth = width
        }
    }

    private func setupCustomizationAttribute(attr:XMLLayoutAttribute.customizationAttribute,value:String,childView:UIView,superView:UIView){
        switch attr {
        case .textColor:
            (childView as? XMLLayoutTextCustomizable)?.setTextColorFromXmlAttribute(color: UIColor.hexStringToUIColor(hex: value))
        case .backgroundColor:
            childView.backgroundColor = UIColor.hexStringToUIColor(hex: value)

        case .alpha:
            guard let alpha = value.asCGFloat else { return }
            childView.alpha = alpha

        case .borderColor:
            childView.layer.borderColor = UIColor.hexStringToUIColor(hex: value).cgColor
        }

    }

    private func setupInsetAttribute(attr:XMLLayoutAttribute.insetAttribute,value:String, childView: UIView,superView:UIView){

        guard let insetValue = value.asCGFloat else { return }
        guard var childView = childView as? XMLLayoutInsetCustomizable else {
            return
        }
        var currentInset = childView.inset
        switch attr{
        case .top:
            currentInset.top = insetValue
        case .bottom:
            currentInset.bottom = insetValue
        case .leading:
            currentInset.left = insetValue
        case .trailing:
            currentInset.right = insetValue
        case .all:
            currentInset.top = insetValue
            currentInset.bottom = insetValue
            currentInset.left = insetValue
            currentInset.right = insetValue
        }
        childView.inset = currentInset
    }

    private func setupLayoutAttribute(attr:XMLLayoutAttribute.layoutAttribute,value:String,childView:UIView,superView:UIView){
        switch attr{

        case .bottom:
            layout(anchor: childView.bottomAnchor,childView:childView, attribute: value)
        case .top:
            layout(anchor: childView.topAnchor,childView:childView, attribute: value)
        case .leading:
            layout(anchor: childView.leadingAnchor,childView:childView,attribute: value)
        case .trailing:
            layout(anchor: childView.trailingAnchor,childView:childView,attribute: value)
        case .centerX:
            layout(anchor: childView.centerXAnchor,childView:childView,attribute: value)
        case .centerY:
            layout(anchor: childView.centerYAnchor,childView:childView,attribute: value)
        }
    }

    private func setupMarginAttribute(attr:XMLLayoutAttribute.marginAttribute,value:String,childView:UIView,superView:UIView){
        guard let constant = value.asCGFloat else {
            return
        }
        switch attr {
        case .top:
            let constraint: NSLayoutConstraint
            if let cstr = superView.constraints.first(where: {$0.firstAnchor == childView.topAnchor}){
                constraint = cstr
            } else {
                constraint = childView.topAnchor.constraint(equalTo: superView.topAnchor)
            }
            constraint.constant = constant
            constraint.isActive = true

        case .bottom:
            let constraint: NSLayoutConstraint
            if let cstr = childView.constraints.first(where: {$0.firstAnchor == childView.bottomAnchor}){
                constraint = cstr
            } else {
                constraint = childView.bottomAnchor.constraint(equalTo: superView.bottomAnchor)
            }
            constraint.constant = -1 * constant
            constraint.isActive = true

        case .leading:
            let constraint: NSLayoutConstraint
            if let cstr = superView.constraints.first(where: {$0.firstAnchor == childView.leadingAnchor}){
                constraint = cstr
            } else {
                constraint = childView.leadingAnchor.constraint(equalTo: superView.leadingAnchor)
            }
            constraint.constant = constant
            constraint.isActive = true

        case .trailing:
            let constraint: NSLayoutConstraint
            if let cstr = childView.constraints.first(where: {$0.firstAnchor == childView.trailingAnchor}){
                constraint = cstr
            } else {
                constraint = childView.trailingAnchor.constraint(equalTo: superView.trailingAnchor)
            }
            constraint.constant = -1 * constant
            constraint.isActive = true

        case .all:
            setupMarginAttribute(attr:.bottom,value:value,childView:childView,superView:superView)
            setupMarginAttribute(attr:.top,value:value,childView:childView,superView:superView)
            setupMarginAttribute(attr:.leading,value:value,childView:childView,superView:superView)
            setupMarginAttribute(attr:.trailing,value:value,childView:childView,superView:superView)
        }
    }

    fileprivate func layoutSubviewsRecursively(_ parent: UIView) {

        parent.subviews.forEach{
            $0.layoutSubviews()
            layoutSubviewsRecursively($0)
        }
    }

    private func setupShadowAttribute(attr:XMLLayoutAttribute.shadowAttribute,value:String,childView:UIView,superView:UIView){
        if superView.layer.sublayers?.first(where: {$0.name == "\(childView.frame).shadow"}) == nil{
            var parent = superView
            while let parentOfParent = parent.superview {
                parent = parentOfParent
                parent.layoutSubviews()
                layoutSubviewsRecursively(parent)
            }
        }
        var radLayer:CALayer? = nil

        if childView.layer.cornerRadius != 0 {
            let radiusLayer = CALayer()
            radiusLayer.cornerRadius = childView.layer.cornerRadius
            radiusLayer.masksToBounds = false
            radiusLayer.borderColor = childView.layer.borderColor
            radiusLayer.borderWidth = childView.layer.borderWidth
            radiusLayer.backgroundColor = childView.backgroundColor?.cgColor
            radiusLayer.frame = childView.frame
            childView.backgroundColor = UIColor.clear

            superView.layer.insertSublayer(radiusLayer, below: childView.layer)
            childView.layer.cornerRadius = 0
            childView.layer.borderWidth = 0
            childView.layer.borderColor = nil
            childView.layer.masksToBounds = false
            radLayer = radiusLayer
        }
        let shadowLayer: CALayer
        if let layer = superView.layer.sublayers?.first(where: {$0.name == "\(childView.frame).shadow"}) {
            shadowLayer = layer
        } else {
            shadowLayer = CALayer()
            shadowLayer.name = "\(childView.frame).shadow"
            shadowLayer.shadowPath = UIBezierPath(roundedRect: childView.frame, cornerRadius: radLayer?.cornerRadius ?? 0).cgPath
            superView.layer.insertSublayer(shadowLayer, below: radLayer)
        }
        shadowLayer.masksToBounds = false
        switch attr {
        case .color:
            shadowLayer.shadowColor = UIColor.hexStringToUIColor(hex: value).cgColor
        case .opacity:
            guard let opacity = Float(value) else {
                return
            }
            shadowLayer.shadowOpacity = opacity
        case .radius:
            guard let radius = value.asCGFloat else {
                return
            }
            shadowLayer.shadowRadius = radius
        case .offsetX:
            guard let offset = value.asCGFloat else {
                return
            }
            shadowLayer.shadowOffset.width = offset

        case .offsetY:
            guard let offset = value.asCGFloat else {
                return
            }
            shadowLayer.shadowOffset.height = offset
        }
    }

    private func layout(anchor: NSLayoutXAxisAnchor, childView: UIView, attribute: String){
        let endConstraintElements = attribute.split(separator: ".")
        let objectId = String(endConstraintElements[0])
        guard  endConstraintElements.count > 1  else {
            return
        }
        let destinationAnchorName = String(endConstraintElements[1])
        guard let object = views[objectId], let destinationAnchor = layoutXAnchor(forName: destinationAnchorName, in: object) else {
            return
        }
        anchor.constraint(equalTo: destinationAnchor).isActive = true
    }

    private func layout(anchor: NSLayoutYAxisAnchor,childView: UIView, attribute: String){
        let endConstraintElements = attribute.split(separator: ".")
        let objectId = String(endConstraintElements[0])
        guard  endConstraintElements.count > 1  else {
            return
        }
        let destinationAnchorName = String(endConstraintElements[1])
        guard let object = views[objectId], let destinationAnchor = layoutYAnchor(forName: destinationAnchorName, in: object) else {
            return
        }
        childView.superview?.updateConstraints()
        anchor.constraint(equalTo: destinationAnchor).isActive = true
    }

    private func layoutXAnchor(forName name:String, in view: UIView) -> NSLayoutXAxisAnchor?{
        switch LayoutXAnchorName(rawValue: name) {
        case .none: return nil
        case .some(let anchor):
            switch anchor {
            case .leading: return view.leadingAnchor
            case .center: return view.centerXAnchor
            case .trailing: return view.trailingAnchor
            }
        }
    }

    private func layoutYAnchor(forName name:String, in view: UIView) -> NSLayoutYAxisAnchor?{
        switch LayoutYAnchorName(rawValue: name) {
        case .none: return nil
        case .some(let anchor):
            switch anchor {
        case .top: return view.topAnchor
        case .center: return view.centerYAnchor
        case .bottom: return view.bottomAnchor
            }

        }
    }

    func viewFor<T:UIView>(id: String) -> T?{
        return views[id] as? T
    }


    private enum LayoutXAnchorName: String{
        case leading
        case center
        case trailing
    }

    private enum LayoutYAnchorName: String{
        case top
        case center
        case bottom
    }
}

private extension String {
    var asBool: Bool? {
        return  Bool(self)
    }

    var asCGFloat: CGFloat? {
        guard let number = NumberFormatter().number(from: self) else {
            return nil
        }
        return CGFloat(number)
    }
}
