import UIKit

public extension UIColor{
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

private var AssociatedObjectKey: UInt8 = 7
public extension UIView{
    public var xmlViewTag: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectKey) as? String
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedObjectKey,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
}

extension XMLLayoutSupportedClasses{

    var view:UIView{
        switch self{
        case .button: return UIButton()
        case .view: return UIView()
        case .label: return UILabel()
        case .imageView:
            return UIImageView()
        }
    }
}

extension UIFont.Weight{
    init?(name:String){
        switch name{
        case "black": self = .black
        case "bold": self = .bold
        case "heavy": self = .heavy
        case "regular": self = .regular
        case "medium": self = .medium
        case "light": self = .light
        case "ultraLight": self = .ultraLight
        case "semibold": self = .semibold
        case "thin": self = .thin
        default:
            return nil
        }
    }
}

extension NSTextAlignment{
    init(name: String){
        switch name {
        case "center" : self = .center
        case "justified" : self = .justified
        case "left" : self = .left
        case "right" : self = .right
        case "natural" : self = .natural
        default:
            self = .natural
        }
    }
}

extension NSLineBreakMode {
    init?(name: String){
        switch name{
        case "wordWrapping" : self = .byWordWrapping
        case "charWrapping" : self = .byCharWrapping
        case "clipping" : self = .byClipping
        case "truncatingHead" : self = .byTruncatingHead
        case "truncatingMiddle" : self = .byTruncatingMiddle
        case "truncatingTail" : self = .byTruncatingTail
        default:
            return nil
        }
    }
}

extension UIView.ContentMode {
    init?(name: String) {
        switch name {
        case "scaleAspectFill": self = .scaleAspectFill
        case "scaleToFill": self = .scaleToFill
        case "scaleAspectFit": self = .scaleAspectFit
        case "redraw": self = .redraw
        case "center": self = .center
        case "top": self = .top
        case "left": self = .left
        case "right": self = .right
        case "bottom": self = .bottom
        case "topLeft": self = .topLeft
        case "topRight": self = .topRight
        case "bottomLeft": self = .bottomLeft
        case "bottomRight": self = .bottomRight
        default:
            return nil
        }
    }
}
